#!/usr/bin/env nextflow

/*
========================================================================================
                3' RNA-Seq
========================================================================================
 @#### Authors
 Marcello Salvi, Lucio di Filippo, Chiara Colantuomo
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
Pipeline overview:
 - 1. : Preprocessing
 	- 1.1: FastQC - for raw sequencing reads quality control
 	- 1.2: Trimmomatic  - raw sequence trimming
 - 2. : Mapping
 	- 2.1 : STAR - Mapping to human genome
    - 2.2 : Samtools - SAM and BAM files processing and stats
 - 3. : counts:
 	- 3.1 : Features Count count
 - 4. : Stats
  - 4.1 : MultiQC - Final html report with stats
 - 5. : Output Description HTML
 ----------------------------------------------------------------------------------------
*/

def helpMessage() {
    log.info"""
    =========================================
    =========================================
    Usage:
    The typical command for running the pipeline is as follows:
    Mandatory arguments:
      --reads                        Path to input data (must be surrounded with quotes).
      --gtf_dir                      Path to genome fasta index

    Options:
      --singleEnd                   Specifies that the input is single end reads

    Trimming options
      --notrim                      Specifying --notrim will skip the adapter trimming step.
      --saveTrimmed                 Save the trimmed Fastq files in the the Results directory.
      --trimmomatic_adapters_file   Adapters index for adapter removal
      --trimmomatic_adapters_parameters Trimming parameters for adapters. <seed mismatches>:<palindrome clip threshold>:<simple clip threshold>. Default 2:30:10
      --trimmomatic_window_length   Window size. Default 4
      --trimmomatic_window_value    Window average quality requiered. Default 20
      --trimmomatic_mininum_length  Minimum length of reads
    Other options:
      --save_unmapped_host          Save the reads that didn't map to host genome
      --outdir                      The output directory where the results will be saved

    """.stripIndent()
}

/*
 * SET UP CONFIGURATION VARIABLES
 */
params.help = false


// Pipeline version
version = '1.0'

// Show help emssage
if (params.help){
    helpMessage()
    exit 0
}

/*
 * Default and custom value for configurable variables
 */




// Stage config files for MULTIQC
params.multiqc_config = "${baseDir}/multiqc_config.yaml"

if (params.multiqc_config){
    multiqc_config = file(params.multiqc_config)
}


if (params.trimmomatic_adapters_file) { truseq = file(params.trimmomatic_adapters_file, checkIfExists: true) } else { exit 1, "TruSeq not found!" }


Channel
      .fromPath(params.idx_dir)
      .ifEmpty { exit 1, "Fasta file not found: ${params.idx_dir}."}
      .set{genome_idx_dir }


// SingleEnd option
params.singleEnd = true

// Validate  mandatory inputs
params.reads = false
if (! params.reads ) exit 1, "Missing reads: $params.reads. Specify path with --reads"

/*
 * Create channel for input files
 */

// Create channel for input reads.
Channel
    .fromPath( params.reads)
    .ifEmpty { exit 1, "Cannot find any reads matching: ${params.reads}\nIf this is single-end data, please specify --singleEnd on the command line." }
    .into { raw_reads_fastqc; raw_reads_trimming }
// Create channel for reference index
if( params.design ){
    Channel
        .fromPath(params.design)
        .ifEmpty { exit 1, "design csv: ${params.design}" }
        .set { ss_ch}
}
if( params.gtf_dir ){
    Channel
        .fromPath(params.gtf_dir)
        .ifEmpty { exit 1, "GTF index not found: ${params.gtf_dir}" }
        .set { gtf_featureCounts}
}
if (params.polyA) { polya = file(params.polyA, checkIfExists: true) } else { exit 1, "PolyA not found!" }


// Header log info
log.info "========================================="
log.info " QuantSeq analysis v${version}"
log.info "========================================="
def summary = [:]
summary['Reads']               = params.reads
summary['Data Type']           = params.singleEnd ? 'Single-End' : 'Paired-End'
summary['Current home']        = "$HOME"
summary['Current user']        = "$USER"
summary['Current path']        = "$PWD"
summary['Working dir']         = workflow.workDir
summary['Output dir']          = params.outdir
summary['Script dir']          = workflow.projectDir
summary['Save Unmapped']        = params.save_unmapped_host
summary['Save Trimmed']        = params.saveTrimmed
summary['Config Profile'] = workflow.profile
log.info summary.collect { k,v -> "${k.padRight(21)}: $v" }.join("\n")
log.info "===================================="

// Check that Nextflow version is up to date enough
// try / throw / catch works for NF versions < 0.25 when this was implemented
nf_required_version = '0.25.0'
try {
    if( ! nextflow.version.matches(">= $nf_required_version") ){
        throw GroovyException('Nextflow version too old')
    }
} catch (all) {
    log.error "====================================================\n" +
              "  Nextflow version $nf_required_version required! You are running v$workflow.nextflow.version.\n" +
              "  Pipeline execution will continue, but things may break.\n" +
              "  Please run `nextflow self-update` to update Nextflow.\n" +
              "============================================================"
}
/*
 * STEP 1.1 - FastQC
 */
process fastqc {

  machineType 'n1-standard-4'
	publishDir "${params.outdir}/fastQC", mode: 'copy',
	saveAs: {filename -> filename.indexOf(".zip") > 0 ? "zips/$filename" : "$filename"}

	input:
	file reads from raw_reads_fastqc.toList()

	output:
	file '*_fastqc.{zip,html}' into fastqc_results
	file '.command.out' into fastqc_stdout

	script:


	"""
  mkdir tmp
  fastqc -t 4 -dir tmp $reads
  rm -rf tmp
	"""
}

process trimming {
	machineType 'n1-standard-4'
	publishDir "${params.outdir}/", mode: 'copy',
		saveAs: {filename ->
			if (filename.indexOf("_trimmed_fastqc.html") > 0) "postTrimQC/$filename"
      else if (filename.indexOf("_trimmed_fastqc.zip") > 0) "postTrimQC/zip/$filename"
			else if (filename.indexOf("_report.txt") > 0) "bbduk/logs/$filename"
      else if (params.saveTrimmed && filename.indexOf("_trimmed.fq.gz")) "bbduk/trimmed_fastq/$filename"
			else null
	}

	input:
	file reads from raw_reads_trimming.collect()
  file truseq1 from Channel.value(truseq)
  file polyA from Channel.value(polya)
	output:
	file '*.fq.gz' into trimmed_reads,trimmed_reads_star

	file '*_fastqc.{zip,html}' into bbduk_fastqc_reports

  file '*_report.txt' into bbduk_results

	script:
	"""
    for file in $reads
    do
        base=\${file%.fastq.gz}_trimmed.fq.gz
        txt=\${file%.fastq.gz}_report.txt
        /opt/bbmap/bbduk.sh in=\$file out=\$base ref=$polyA,$truseq1 k=${params.k} ktrim=${params.ktrim} useshortkmers=${params.useshortkmers} mink=${params.mink} qtrim=${params.qtrim} trimq=${params.trimq} minlength=${params.minlength} overwrite=${params.overwrite} 2> \$txt
    done
    mkdir tmp
    fastqc -t 4 -q *.fq.gz
    rm -rf tmp
 	"""
}
process STAR {
	machineType 'e2-standard-16'
  label 'bigTask'
	tag "$prefix"


 publishDir "${params.outdir}/", mode: 'copy',
		saveAs: {filename ->
      if (filename.indexOf("_Log") > 0) "STAR/$prefix/$filename"
      else if (filename.indexOf("_Unmapped.out.mate1") > 0) "STAR/$prefix/Unmapped/$filename"
      else if (filename.indexOf("_Aligned.sortedByCoord.out") > 0) "STAR/$prefix/$filename"
      else null
	}

	input:
    file readsR from trimmed_reads_star.flatten()
    file refhost from genome_idx_dir.collect()


	output:

    file "*.bam" into star_for_bam
    file "*.out"    into     star_results
    file "*Unmapped*" into u_bam_ch
    file "*bam.bai" into bai_ch
    file '*_flagstat.txt' into mapping_host_flagstat

    script:
	prefix = readsR.getName().replaceAll(/(_S[0-9]{2})?(_L00[1-9])?(.R1)?(_1)?(_R1)?(.trimmed)?(_val_1)?(_00[1-2])?(\.fq)?(\.fastq)?(\.gz)?/,"")

    """
    STAR --runThreadN 16\
    --genomeDir $refhost \
    --readFilesCommand zcat \
    --readFilesIn ${readsR} \
    --outFilterType ${params.outFilterType}\
    --outSAMtype ${params.outSAMtype}\
    --outReadsUnmapped ${params.outReadsUnmapped}\
    --outFilterMultimapNmax ${params.outFilterMultimapNmax}\
    --alignSJoverhangMin ${params.alignSJoverhangMin}\
    --alignSJDBoverhangMin ${params.alignSJDBoverhangMin}\
    --outFilterMismatchNmax ${params.outFilterMismatchNmax}\
    --outFilterMismatchNoverLmax ${params.outFilterMismatchNoverLmax}\
    --alignIntronMin ${params.alignIntronMin}\
    --alignIntronMax ${params.alignIntronMax}\
    --alignMatesGapMax ${params.alignMatesGapMax}\
    --outFilterScoreMinOverLread ${params.outFilterScoreMinOverLread}\
    --outFilterMatchNminOverLread ${params.outFilterMatchNminOverLread}\
    --outSAMattributes NH HI NM MD \
    --outFileNamePrefix ./${prefix}"_"
    samtools index ${prefix}"_Aligned.sortedByCoord.out.bam"
    samtools flagstat ./${prefix}"_Aligned.sortedByCoord.out.bam" > ./${prefix}"_flagstat.txt"
	"""
}
process htseqCount {
tag "${aligned_reads.baseName - '.sorted'}"
machineType 'n1-standard-1'
publishDir "${params.outdir}/htseqCount", mode: 'copy',
		    saveAs: {filename ->
			if (filename.indexOf("_unique_counts.txt.gz") > 0) "Unique/$filename"
			else if (filename.indexOf("_multimap_counts.txt.gz") > 0) "Multimap/$filename"
      else if (filename.indexOf(".log") > 0) "logs/$filename"
	}

	input:
    file aligned_reads from star_for_bam
    file gtf from gtf_featureCounts.collect()


	output:
  file "*_unique_counts.txt.gz" into unique_counts_ch
  file "*_multimap_counts.txt.gz" into multimap_ch
  script:
  prefix = aligned_reads.getName().replaceAll(/_Aligned.sortedByCoord.out.bam/,"")
  """
    ucountf=${prefix}"_unique_counts.txt.gz"
    mcountf=${prefix}"_multimap_counts.txt.gz"
    htseq-count -m intersection-nonempty -s ${params.stranded_lib} -f bam -r pos $aligned_reads $gtf -a 10 | gzip> \$ucountf
    htseq-count -m intersection-nonempty -s ${params.stranded_lib} -f bam -r pos $aligned_reads $gtf -a 0 | gzip > \$mcountf
  """
}

process merge_htseqCounts {
container 'gcr.io/ngdx-cloud-resources/demultiplex'

tag "${input_files[0].baseName - '.sorted'}"
publishDir "${params.outdir}/htseqCount", mode: 'copy'

input:
file input_files from unique_counts_ch.collect()
file design from ss_ch
output:
file '*_rawCounts.txt' into htseqcounts_merged

script:

"""
  Rscript --vanilla nextflow-bin/htseqRawTable.R '$design' 'TRUE'
"""
}

process multiqc {
tag "$prefix"
publishDir path: { "${params.outdir}/MultiQC" }, mode: 'copy'

input:
file multiqc_config from multiqc_config
file (fastqc:'fastqc/*') from fastqc_results.collect().ifEmpty([])
file ('bbduk/*') from bbduk_results.collect()
file ('bbduk-fastqc/*') from bbduk_fastqc_reports.collect()
file ('star/*') from star_results.collect()
file ('samtools/*') from mapping_host_flagstat.collect()


output:
file '*multiqc_report.html' into multiqc_report
file '*_data' into multiqc_data
val prefix into multiqc_prefix

script:
prefix = fastqc[0].toString() - '_fastqc.html' - 'fastqc/'

"""
multiqc -d . --config $multiqc_config
"""
}
